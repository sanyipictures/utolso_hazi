﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Repo.GenericRepository;

namespace TW.Repo.PlayerRepository
{
    public class PlayerCrud : Crud<Player>, IPlayerCrud
    {
        public PlayerCrud(DbContext ctx) : base(ctx)
        {
        }

        public override Player GetByID(int id)
        {
            return this.Get(x => x.Id == id).SingleOrDefault();
        }

        public Player GetByUsername(string uname)
        {
            return this.Get(x => x.NAME == uname).SingleOrDefault();
        }

        public void Modify(int id, PLAYERATTRIBUTES attribute, string value)
        {
            Player current = this.GetByID(id);
            if (current == null)
            {
                throw new ArgumentNullException("NO DATA IN DATABASE");
            }
            else
            {
                if (value != null)
                {
                    if (attribute == PLAYERATTRIBUTES.EMAIL)
                    {
                        current.EMAIL = value;
                    }
                    if (attribute == PLAYERATTRIBUTES.PASSWORD)
                    {
                        current.PASSWORD = value;
                    }
                    if (attribute == PLAYERATTRIBUTES.USERNAME)
                    {
                        current.NAME = value;
                    }
                }
                this.context.SaveChanges();
            }
        }
    }
}
