﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Repo.GenericRepository;

namespace TW.Repo.PlayerRepository
{
    public enum PLAYERATTRIBUTES { USERNAME, EMAIL, PASSWORD }

    public interface IPlayerCrud : ICrud<Player>
    {
        Player GetByUsername(string uname);
        void Modify(int id, PLAYERATTRIBUTES attribute, string value);
    }
}
