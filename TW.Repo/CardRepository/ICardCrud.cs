﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Repo.GenericRepository;

namespace TW.Repo.CardRepository
{
    public interface ICardCrud : ICrud<Cards>
    {
        List<Cards> GetByUserID(int id);
        void Modify(int id, int playerID);
    }
}
