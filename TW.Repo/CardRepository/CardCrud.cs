﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Repo.GenericRepository;

namespace TW.Repo.CardRepository
{
    public class CardCrud : Crud<Cards>, ICardCrud
    {
        public CardCrud(DbContext ctx) : base(ctx)
        {
        }

        public override Cards GetByID(int id)
        {
            return this.Get(x => x.Id == id).SingleOrDefault();
        }

        public List<Cards> GetByUserID(int id)
        {
            return this.Get(x => x.PLAYERID == id).ToList();
        }

        public void Modify(int id, int playerID)
        {
            Cards actual = this.GetByID(id);
            actual.PLAYERID = playerID;
            this.context.SaveChanges();
        }
    }
}
