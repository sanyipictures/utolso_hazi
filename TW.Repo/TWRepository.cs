﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Repo.CardRepository;
using TW.Repo.PlayerRepository;

namespace TW.Repo
{
    public class TWRepository
    {
        public IPlayerCrud PlayerRepo { get; private set; }
        public ICardCrud CardRepo { get; private set; }

        public TWRepository(IPlayerCrud newplayer, ICardCrud newcard)
        {
            this.PlayerRepo = newplayer;
            this.CardRepo = newcard;
        }
    }
}
