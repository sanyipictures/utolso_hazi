﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TW.Web.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:62063/api/tw/";
            Console.WriteLine("Waiting..................");

            Console.ReadKey();

            using (WebClient client = new WebClient())
            {
                Console.WriteLine("Registering!");

                NameValueCollection postData;
                byte[] responeBytes;
                postData = new NameValueCollection();
                postData.Add("NAME", "Ancsi");
                postData.Add("EMAIL", "Ancs@Ancsuka.ru");
                postData.Add("PASSWORD", "asdasd");
                responeBytes = client.UploadValues(url + "register", "POST", postData);

                Console.WriteLine("register: " + Encoding.UTF8.GetString(responeBytes));

                postData = new NameValueCollection();

                Console.WriteLine("Logging in: ");

                postData.Add("NAME", "Ancsi");
                postData.Add("PASSWORD", "asdasd");

                responeBytes = client.UploadValues(url + "register", "POST", postData);
                Console.WriteLine("Login: " + Encoding.UTF8.GetString(responeBytes));

                postData = new NameValueCollection();

                Console.WriteLine("Delete: ");

                postData.Add("NAME", "Ancsi");
                postData.Add("EMAIL", "Ancs@Ancsuka.ru");
                postData.Add("PASSWORD", "asdasd");

                responeBytes = client.UploadValues(url + "delete", "POST", postData);
                Console.WriteLine("DELETING: " + Encoding.UTF8.GetString(responeBytes));

            }
            Console.WriteLine("DONE");
            Console.ReadKey();
        }
    }
}
