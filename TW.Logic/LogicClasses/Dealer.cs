﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Logic.LogicInterfaces;

namespace TW.Logic.LogicClasses
{
    public class Dealer : IDealer<Cards>
    {
        public List<Cards> Deal(int amount, List<Cards> deck, Func<List<Cards>, List<Cards>> func)
        {
            func = (cards) => {

                List<Cards> takenCards = cards.Take(amount).ToList();
                cards.RemoveRange(0, amount);
                return takenCards;
            };
            return func(deck);
        }

        public List<Cards> Shuffle(List<Cards> items, Func<List<Cards>, List<Cards>> func)
        {
            func = (cards) =>
            {
                Random rnd = new Random();
                Cards tmp;
                int size = cards.Count();
                int position = 0;
                for (int i = 0; i < size; i++)
                {
                    tmp = cards[i];
                    cards[i] = cards[position = rnd.Next(size)];
                    cards[position] = tmp;
                }

                return cards;
            };
            return func(items);
        }

        public List<Cards> Shuffle(List<Cards> items, Action<List<Cards>> shuffleFunc)
        {
            shuffleFunc = (cards) =>
            {
                Random rnd = new Random();
                Cards tmp;
                int size = cards.Count();
                int position = 0;
                for (int i = 0; i < size; i++)
                {
                    tmp = cards[i];
                    cards[i] = cards[position = rnd.Next(size)];
                    cards[position] = tmp;
                }

            };
            return items;
        }

    }
}
