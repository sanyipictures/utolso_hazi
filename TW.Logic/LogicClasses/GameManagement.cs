﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Logic.LogicEntities;
using TW.Logic.LogicInterfaces;
using TW.Repo;
using TW.Repo.CardRepository;
using TW.Repo.PlayerRepository;

namespace TW.Logic.LogicClasses
{
    public class GameManagement : ILogic
    {
        protected TWRepository repo;
        IMapper mapper;

        public GameManagement() {
            TWDatabaseEntities ED = new TWDatabaseEntities();
            var newplayer = new PlayerCrud(ED);
            var newcrud = new CardCrud(ED);
            this.repo = new TWRepository(newplayer, newcrud);
            this.mapper = LogicMapperFactory.CreateMapper();
        }

        public GameManagement(IPlayerCrud newplayer, ICardCrud newcrud)
        {
            this.repo = new TWRepository(newplayer, newcrud);
            this.mapper = LogicMapperFactory.CreateMapper();
        }

        public void DeleteProfile(LogicPlayer userDTO)
        {
            this.repo.PlayerRepo.Delete(userDTO.Id);
        }

        public void GiveCards(int amount, LogicPlayer player)
        {
            Dealer dealer = new Dealer();
            List <Cards> crds = this.repo.CardRepo.GetAll().ToList();
            crds = dealer.Deal(amount, crds, x => crds);

            foreach (Cards item in crds)
            {
                this.repo.CardRepo.Modify(item.Id, player.Id);
            }

        }

        public LogicPlayer Login(LogicPlayer userDTO)
        {
            Player current = this.repo.PlayerRepo.GetByUsername(userDTO.NAME);
            if (current == null) {
                throw new Exception("NOT REGISTERED!!!!");
            }
            return this.mapper.Map<Player, LogicPlayer>(current);
        }

        public LogicDTO ManageActivities(ACTIVITYTYPE activity, LogicPlayer userDTO)
        {
            if (activity.Equals(ACTIVITYTYPE.REGISTER)) {
                this.Register(userDTO);
                return new LogicDTO() { Player = this.mapper.Map<Player,LogicPlayer>(this.repo.PlayerRepo.GetByUsername(userDTO.NAME)), Cards = new List<LogicCard>()};
            }
            if (activity.Equals(ACTIVITYTYPE.LOGIN))
            {
                return new LogicDTO() { Player = this.Login(userDTO), Cards = new List<LogicCard>() };
            }
            if (activity.Equals(ACTIVITYTYPE.LOGIN))
            {
                this.DeleteProfile(userDTO);
                return new LogicDTO() { Player = new LogicPlayer() { Id = 0, EMAIL = "DELETED", NAME = "DELETED", PASSWORD = "DELETED", STATUS = -1}, Cards = new List<LogicCard>() };
            }
            if (activity.Equals(ACTIVITYTYPE.START))
            {
                this.Reset();
                this.GiveCards(2,userDTO);
                return new LogicDTO() { Player = this.mapper.Map < Player, LogicPlayer > (this.repo.PlayerRepo.GetByUsername(userDTO.NAME)),
                    Cards = this.mapper.Map<List<Cards>,List<LogicCard>>(this.repo.CardRepo.GetByUserID(userDTO.Id)) };
            }
            if (activity.Equals(ACTIVITYTYPE.ASKCARD))
            {
                this.GiveCards(1, userDTO);
                return new LogicDTO()
                {
                    Player = this.mapper.Map<Player, LogicPlayer>(this.repo.PlayerRepo.GetByUsername(userDTO.NAME)),
                    Cards = this.mapper.Map<List<Cards>, List<LogicCard>>(this.repo.CardRepo.GetByUserID(userDTO.Id))
                };
            }
            if (activity.Equals(ACTIVITYTYPE.DONE))
            {
                return this.TellWin(userDTO);
            }
            return null;
        }

        public void Register(LogicPlayer userDTO)
        {
            var smtg = this.repo.PlayerRepo.GetAll();

            var stg2 = this.mapper.Map<IQueryable<Player>, List<LogicPlayer>>(smtg);
            userDTO.Id = ++stg2.LastOrDefault().Id;
            userDTO.STATUS = 0;
            this.repo.PlayerRepo.Insert(this.mapper.Map<LogicPlayer, Player>(userDTO));
        }

        public void Reset()
        {
            Dealer dealer = new Dealer();

            foreach (var item in this.repo.CardRepo.GetAll())
            {
                this.repo.CardRepo.Delete(item.Id);
            }
            List<Cards> newDeck = new List<Cards>(); 
            for(int i = 0; i < Deck.deckCards.Capacity; ++i)
            {
                newDeck.Add(new Cards() { PLAYERID = -1, CARDNAME = Deck.deckCards[i], Id = i });
            }
            
            foreach (Cards item in dealer.Shuffle(newDeck, x => newDeck))
            {
                this.repo.CardRepo.Insert(item);
            }
        }

        public LogicDTO TellWin(LogicPlayer userDTO)
        {
            List<Cards> cards = this.repo.CardRepo.GetByUserID(userDTO.Id);
            int sum = 0;

            foreach (Cards item in cards)
            {
                if (item.CARDNAME.EndsWith("2")) {
                    sum += 2;
                    continue;
                }
                if (item.CARDNAME.EndsWith("3"))
                {
                    sum += 3;
                    continue;
                }
                if (item.CARDNAME.EndsWith("4"))
                {
                    sum += 4;
                    continue;
                }
                if (item.CARDNAME.EndsWith("5"))
                {
                    sum += 5;
                    continue;
                }
                if (item.CARDNAME.EndsWith("6"))
                {
                    sum += 6;
                    continue;
                }
                if (item.CARDNAME.EndsWith("7"))
                {
                    sum += 7;
                    continue;
                }
                if (item.CARDNAME.EndsWith("8"))
                {
                    sum += 8;
                    continue;
                }
                if (item.CARDNAME.EndsWith("9"))
                {
                    sum += 9;
                    continue;
                }
                if (item.CARDNAME.EndsWith("10"))
                {
                    sum += 10;
                    continue;
                }
                if (item.CARDNAME.EndsWith("J") || item.CARDNAME.EndsWith("Q") || item.CARDNAME.EndsWith("K") || item.CARDNAME.EndsWith("A"))
                {
                    sum += 10;
                }
            }
            if (sum >= 18 && sum <= 21)
            {
                userDTO.STATUS = 2;
                return new LogicDTO() { Cards = this.mapper.Map<List<Cards>, List<LogicCard>>(cards), Player = userDTO };
            }
            else {
                userDTO.STATUS = 3;
                return new LogicDTO() { Cards = this.mapper.Map<List<Cards>, List<LogicCard>>(cards), Player = userDTO };
            }
        }
    }
}
