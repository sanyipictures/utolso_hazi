﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Logic.LogicEntities;

namespace TW.Logic
{
    public class LogicDTO
    {
        public LogicPlayer Player { get; set; }
        public List<LogicCard> Cards { get; set; }
    }
}
