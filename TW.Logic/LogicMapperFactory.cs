﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Logic.LogicEntities;

namespace TW.Logic
{
    public class LogicMapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Cards, LogicCard>().ReverseMap();
                cfg.CreateMap<Player, LogicPlayer>().ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
