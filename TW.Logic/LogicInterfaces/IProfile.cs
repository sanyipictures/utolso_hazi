﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Logic.LogicInterfaces
{
    public interface IProfile<TLogicEntity> where TLogicEntity : class
    {
        TLogicEntity Login(TLogicEntity userDTO);
        void Register(TLogicEntity userDTO);
        void DeleteProfile(TLogicEntity userDTO);
    }
}
