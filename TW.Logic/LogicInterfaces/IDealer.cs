﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Logic.LogicInterfaces
{
    public interface IDealer<TLogicItem>
    {
        List<TLogicItem> Shuffle(List<TLogicItem> items, Func<List<TLogicItem>, List<TLogicItem>> func);
        List<TLogicItem> Deal(int amount, List<TLogicItem> deck, Func<List<TLogicItem>, List<TLogicItem>> func);
    }
}
