﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Logic.LogicEntities;

namespace TW.Logic.LogicInterfaces
{
    public interface ILogic : IGamePlay<LogicPlayer, LogicDTO>, IProfile<LogicPlayer>
    {

    }
}
