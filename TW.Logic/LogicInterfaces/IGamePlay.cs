﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Logic.LogicInterfaces
{
    public enum ACTIVITYTYPE{ ASKCARD, DONE, LOGIN, DELETE, REGISTER, START }

    public interface IGamePlay<TInteracter, TDTO> where TInteracter : class
    {
        TDTO ManageActivities(ACTIVITYTYPE activity, TInteracter userDTO);
        void GiveCards(int amount, TInteracter player);
        void Reset();
        TDTO TellWin(TInteracter userDTO);
    }
}
