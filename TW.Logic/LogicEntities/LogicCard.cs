﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Logic.LogicEntities
{
    public class LogicCard
    {
        public int Id { get; set; }
        public Nullable<int> PLAYERID { get; set; }
        public string CARDNAME { get; set; }
    }
}
