﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.Logic
{
    public class Deck
    {
        public static List<String> deckCards = new List<String>() {
            "spades2", "spades3", "spades4", "spades5", "spades6", "spades7", "spades8", "spades9", "spades10",
            "spadesJ", "spadesQ", "spadesK", "spadesA", "hearts2", "hearts3", "hearts4", "hearts5", "hearts6",
            "hearts7", "hearts8", "hearts9", "hearts10", "heartsJ", "heartsQ", "heartsK", "heartsA", "diamonds2",
            "diamonds3", "diamonds4", "diamonds5", "diamonds6", "diamonds7", "diamonds8", "diamonds9", "diamonds10",
            "diamondsJ", "diamondsQ", "diamondsK", "diamondsA"
        };
    }
}
