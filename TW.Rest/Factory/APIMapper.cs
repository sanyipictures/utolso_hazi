﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TW.Logic.LogicEntities;
using TW.Rest.DTOs;

namespace TW.Rest.Factory
{
    public class APIMapper
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<RegisterDTO, LogicPlayer>().ReverseMap();
                cfg.CreateMap<DeleteDTO, LogicPlayer>().ReverseMap();
                cfg.CreateMap<LoginDTO, LogicPlayer>().ReverseMap();
                cfg.CreateMap<LogicPlayer, ProfileDTO>().ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}