﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TW.Rest.DTOs
{
    public class ProfileDTO
    {
        public int ID { get; set; }
        public string NAME { get; set; }
    }
}