﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TW.Rest.DTOs
{
    public class LoginDTO
    {
        public string NAME { get; set; }
        public string PASSWORD { get; set; }
    }
}