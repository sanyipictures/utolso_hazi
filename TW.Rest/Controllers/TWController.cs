﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TW.Logic.LogicClasses;
using TW.Logic.LogicEntities;
using TW.Logic.LogicInterfaces;
using TW.Rest.DTOs;
using TW.Rest.Factory;

namespace TW.Rest.Controllers
{
    public class TWController : ApiController
    {
        ILogic logic;
        IMapper mapper;

        public TWController()
        {
            this.logic = new GameManagement();
            this.mapper = APIMapper.CreateMapper();
        }
        [HttpPost]
        [ActionName("register")]
        public string Register(RegisterDTO income) {
            LogicPlayer lplayer = this.mapper.Map<RegisterDTO, LogicPlayer>(income);
            this.logic.Register(lplayer);
            return "done";
        }

        [HttpPost]
        [ActionName("delete")]
        public string Delete(DeleteDTO income)
        {
            LogicPlayer lplayer = this.mapper.Map<DeleteDTO, LogicPlayer>(income);
            this.logic.DeleteProfile(lplayer);
            return "done";
        }

        [HttpPost]
        [ActionName("login")]
        public ProfileDTO Login(LoginDTO income)
        {
            LogicPlayer lplayer = this.mapper.Map<LoginDTO, LogicPlayer>(income);
            lplayer = this.logic.Login(lplayer);
            return this.mapper.Map<LogicPlayer, ProfileDTO>(lplayer);
        }
    }
}
